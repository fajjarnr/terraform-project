provider "aws" {
  region = "ap-southeast-3"
}

variable "cidr_blocks" {
  description = "cidr block for vpc and subnet"
  # type        = list(string)
  type = list(object({
    cidr_block = string
    name       = string
  }))
}

variable "subnet_cidr_block" {
  description = "cidr for subnet"
  default     = "10.0.10.0/24"
}

variable "vpc_cidr_block" {
  description = "vpc cidr block"
}

variable "enviroment" {
  description = "enviroment variable"
}

resource "aws_vpc" "dev-vpc" {
  cidr_block = var.cidr_blocks[0].cidr_block

  tags = {
    Name = var.cidr_blocks[0].name
  }
}

resource "aws_subnet" "dev-subnet-1" {
  vpc_id            = aws_vpc.dev-vpc.id
  cidr_block        = var.cidr_blocks[1].cidr_block
  availability_zone = "ap-southeast-3a"

  tags = {
    Name = var.cidr_blocks[1].name
  }
}

data "aws_vpc" "existing_vpc" {
  default = true
}

resource "aws_subnet" "dev-subnet-2" {
  vpc_id            = data.aws_vpc.existing_vpc.id
  cidr_block        = "172.31.48.0/20"
  availability_zone = "ap-southeast-3a"

  tags = {
    Name = "dev-subnet-2"
  }
}

output "dev-vpc-id" {
  value = aws_vpc.dev-vpc.id
}

output "dev-subnet-1" {
  value = aws_subnet.dev-subnet-1.id
}
